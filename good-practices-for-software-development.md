# Answer 1
I think using tools like Loom for showing my screen and GitHub Gists for sharing code is a great idea. They can help make communication clearer, so I'm going to start using them.

# Answer 2
When there is something in my field that I don't know much about, I'll set aside time to learn, take online courses, and ask experienced team members for help.